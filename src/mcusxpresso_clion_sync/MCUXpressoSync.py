import json
from pathlib import Path
from typing import Union, List

from mcusxpresso_clion_sync.CLionCMakeSettings import CLionCMakeSettings
from mcusxpresso_clion_sync.VsCodeCmakeKits import VsCodeCmakeKits_from_dict, VsCodeCmakeKits


class MCUXpressoSync:
    def __init__(self, project_dir: Union[str, Path]):
        if isinstance(project_dir, Path):
            self.project_dir = project_dir
        else:
            self.project_dir = Path(project_dir)
        if not self.project_dir.is_dir():
            raise Exception(f'"{project_dir}" is not a folder')

        self.project_dir = self.project_dir.absolute().resolve()

        if not self.cmake_kits_file_path.is_file():
            raise Exception(f'"{self.cmake_kits_file_path}" is not an existing file')

        self.cmake_setting = CLionCMakeSettings(self.cmake_settings_path)

    def run(self):
        cmake_kits = self.read_cmake_kits_file()
        self.cmake_setting.reset()
        for kit in cmake_kits:
            config = self.cmake_setting.add_cmake_config(kit.name)
            config.config_name = 'Debug'
            config.enabled = True
            config.toolchain_name = 'ARM'
            config.toolchain_file = kit.toolchain_file
            config.library_type = kit.cmake_settings.library_type
            config.language = kit.cmake_settings.language
            config.debug_console = kit.cmake_settings.debug_console
            config.postprocess_util = kit.cmake_settings.postprocess_utility
            config.sdk_root_dir_path = kit.cmake_settings.sdk_root_dir_path
            config.env['ARMGCC_DIR'] = kit.environment_variables.armgcc_dir
            config.env['SdkRootDirPath'] = kit.environment_variables.sdk_root_dir_path

        self.cmake_setting.write()

    def read_cmake_kits_file(self) -> List[VsCodeCmakeKits]:
        cmake_kits = json.loads(self.cmake_kits_file_path.read_text())
        return VsCodeCmakeKits_from_dict(cmake_kits)

    @property
    def cmake_kits_file_path(self) -> Path:
        return self.project_dir / '.vscode/cmake-kits.json'

    @property
    def cmake_settings_path(self) -> Path:
        return self.project_dir / '.idea/cmake.xml'
