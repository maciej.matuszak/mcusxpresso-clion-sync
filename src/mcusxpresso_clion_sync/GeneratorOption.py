from typing import Optional


class GeneratorOption:

    def __init__(self, prefix: str, key: str, type_: Optional[str] = None, value: Optional[str] = None):
        self.prefix: str = prefix
        self.key: str = key
        self.type = type_
        self.value = value
