from typing import Dict, Optional

from bs4 import Tag, BeautifulSoup

from mcusxpresso_clion_sync.GeneratorOption import GeneratorOption


class CLionCMakeConfig:

    def __init__(self):
        self.generator_options: Dict[str, GeneratorOption] = {}
        self.set_gen_option('--', 'no-warn-unused-cli')
        self.set_gen_option('-D', 'CMAKE_EXPORT_COMPILE_COMMANDS', type_='BOOL', value='TRUE')

        self.profile_name = ''
        self.generator = 'Ninja'
        self.toolchain_name = 'ARM'
        self.enabled = True
        self.env: Dict[str, str] = {}

    def set_gen_option(self, prefix: str, key: str, type_: Optional[str] = None, value: Optional[str] = None):
        self.generator_options[key] = GeneratorOption(prefix, key, type_=type_, value=value)

    @property
    def generator(self):
        opt = self.generator_options.get('G', None)
        return opt.value if opt else None

    @generator.setter
    def generator(self, value):
        self.set_gen_option('-G ', value)

    @property
    def config_name(self):
        opt = self.generator_options.get('CMAKE_BUILD_TYPE', None)
        return opt.value if opt else None

    @config_name.setter
    def config_name(self, value):
        self.set_gen_option('-D', 'CMAKE_BUILD_TYPE', type_='STRING', value=value)

    @property
    def toolchain_file(self):
        opt = self.generator_options.get('CMAKE_TOOLCHAIN_FILE', None)
        return opt.value if opt else None

    @toolchain_file.setter
    def toolchain_file(self, value):
        self.set_gen_option('-D', 'CMAKE_TOOLCHAIN_FILE', type_='FILEPATH', value=value)

    @property
    def library_type(self):
        opt = self.generator_options.get('LIBRARY_TYPE', None)
        return opt.value if opt else None

    @library_type.setter
    def library_type(self, value):
        self.set_gen_option('-D', 'LIBRARY_TYPE', type_='STRING', value=value)

    @property
    def language(self):
        opt = self.generator_options.get('LANGUAGE', None)
        return opt.value if opt else None

    @language.setter
    def language(self, value):
        self.set_gen_option('-D', 'LANGUAGE', type_='STRING', value=value)

    @property
    def debug_console(self):
        opt = self.generator_options.get('DEBUG_CONSOLE', None)
        return opt.value if opt else None

    @debug_console.setter
    def debug_console(self, value):
        self.set_gen_option('-D', 'DEBUG_CONSOLE', type_='STRING', value=value)

    @property
    def postprocess_util(self):
        opt = self.generator_options.get('POSTPROCESS_UTILITY', None)
        return opt.value if opt else None

    @postprocess_util.setter
    def postprocess_util(self, value):
        self.set_gen_option('-D', 'POSTPROCESS_UTILITY', type_='FILEPATH', value=value)

    @property
    def sdk_root_dir_path(self):
        opt = self.generator_options.get('SdkRootDirPath', None)
        return opt.value if opt else None

    @sdk_root_dir_path.setter
    def sdk_root_dir_path(self, value):
        self.set_gen_option('-D', 'SdkRootDirPath', type_='FILEPATH', value=value)

    @property
    def generator_options_string(self) -> str:
        options = ''
        for opt in self.generator_options.values():

            options += f' {opt.prefix}{opt.key}'
            if opt.type is not None:
                options += f':{opt.type}'

            if opt.value is not None:
                if opt.type == 'FILEPATH':
                    options += f'="{opt.value}"'
                else:
                    options += f'={opt.value}'

        return options

    def update_xml(self, bs: BeautifulSoup, config: Tag):
        config.attrs['PROFILE_NAME'] = self.profile_name
        config.attrs['GENERATION_DIR'] = self.config_name
        config.attrs['ENABLED'] = 'true' if self.enabled else 'false'
        config.attrs['CONFIG_NAME'] = self.config_name
        config.attrs['TOOLCHAIN_NAME'] = self.toolchain_name
        config.attrs['GENERATION_OPTIONS'] = self.generator_options_string
        if len(self.env):
            age = bs.new_tag('ADDITIONAL_GENERATION_ENVIRONMENT')
            config.append(age)
            envs = bs.new_tag('envs')
            age.append(envs)
            for k, v in self.env.items():
                env = bs.new_tag('env', attrs={
                    'name': k,
                    'value': v
                })
                envs.append(env)
