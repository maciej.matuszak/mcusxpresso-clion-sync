# To use this code, make sure you
# generated using: https://codebeautify.org/json-to-python-pojo-generator
#
#     import json
#
# and then, to convert JSON from a string, do
#
#     result = VsCodeCmakeKits_from_dict(json.loads(json_string))

from typing import Any, List, TypeVar, Type, cast, Callable

T = TypeVar("T")


def from_str(x: Any) -> str:
    assert isinstance(x, str)
    return x


def from_bool(x: Any) -> bool:
    assert isinstance(x, bool)
    return x


def to_class(c: Type[T], x: Any) -> dict:
    assert isinstance(x, c)
    return cast(Any, x).to_dict()


def from_list(f: Callable[[Any], T], x: Any) -> List[T]:
    assert isinstance(x, list)
    return [f(y) for y in x]


class CmakeSettings:
    library_type: str
    language: str
    debug_console: str
    postprocess_utility: str
    sdk_root_dir_path: str

    def __init__(self, library_type: str, language: str, debug_console: str, postprocess_utility: str,
                 sdk_root_dir_path: str) -> None:
        self.library_type = library_type
        self.language = language
        self.debug_console = debug_console
        self.postprocess_utility = postprocess_utility
        self.sdk_root_dir_path = sdk_root_dir_path

    @staticmethod
    def from_dict(obj: Any) -> 'CmakeSettings':
        assert isinstance(obj, dict)
        library_type = from_str(obj.get("LIBRARY_TYPE"))
        language = from_str(obj.get("LANGUAGE"))
        debug_console = from_str(obj.get("DEBUG_CONSOLE"))
        postprocess_utility = from_str(obj.get("POSTPROCESS_UTILITY"))
        sdk_root_dir_path = from_str(obj.get("SdkRootDirPath"))
        return CmakeSettings(library_type, language, debug_console, postprocess_utility, sdk_root_dir_path)

    def to_dict(self) -> dict:
        result: dict = {}
        result["LIBRARY_TYPE"] = from_str(self.library_type)
        result["LANGUAGE"] = from_str(self.language)
        result["DEBUG_CONSOLE"] = from_str(self.debug_console)
        result["POSTPROCESS_UTILITY"] = from_str(self.postprocess_utility)
        result["SdkRootDirPath"] = from_str(self.sdk_root_dir_path)
        return result


class EnvironmentVariables:
    armgcc_dir: str
    sdk_root_dir_path: str

    def __init__(self, armgcc_dir: str, sdk_root_dir_path: str) -> None:
        self.armgcc_dir = armgcc_dir
        self.sdk_root_dir_path = sdk_root_dir_path

    @staticmethod
    def from_dict(obj: Any) -> 'EnvironmentVariables':
        assert isinstance(obj, dict)
        armgcc_dir = from_str(obj.get("ARMGCC_DIR"))
        sdk_root_dir_path = from_str(obj.get("SdkRootDirPath"))
        return EnvironmentVariables(armgcc_dir, sdk_root_dir_path)

    def to_dict(self) -> dict:
        result: dict = {}
        result["ARMGCC_DIR"] = from_str(self.armgcc_dir)
        result["SdkRootDirPath"] = from_str(self.sdk_root_dir_path)
        return result


class VsCodeCmakeKits:
    name: str
    environment_variables: EnvironmentVariables
    cmake_settings: CmakeSettings
    keep: bool
    toolchain_file: str

    def __init__(self, name: str, environment_variables: EnvironmentVariables, cmake_settings: CmakeSettings,
                 keep: bool, toolchain_file: str) -> None:
        self.name = name
        self.environment_variables = environment_variables
        self.cmake_settings = cmake_settings
        self.keep = keep
        self.toolchain_file = toolchain_file

    @staticmethod
    def from_dict(obj: Any) -> 'VsCodeCmakeKits':
        assert isinstance(obj, dict)
        name = from_str(obj.get("name"))
        environment_variables = EnvironmentVariables.from_dict(obj.get("environmentVariables"))
        cmake_settings = CmakeSettings.from_dict(obj.get("cmakeSettings"))
        keep = from_bool(obj.get("keep"))
        toolchain_file = from_str(obj.get("toolchainFile"))
        return VsCodeCmakeKits(name, environment_variables, cmake_settings, keep, toolchain_file)

    def to_dict(self) -> dict:
        result: dict = {}
        result["name"] = from_str(self.name)
        result["environmentVariables"] = to_class(EnvironmentVariables, self.environment_variables)
        result["cmakeSettings"] = to_class(CmakeSettings, self.cmake_settings)
        result["keep"] = from_bool(self.keep)
        result["toolchainFile"] = from_str(self.toolchain_file)
        return result


def VsCodeCmakeKits_from_dict(s: Any) -> List[VsCodeCmakeKits]:
    return from_list(VsCodeCmakeKits.from_dict, s)


def VsCodeCmakeKits_to_dict(x: List[VsCodeCmakeKits]) -> Any:
    return from_list(lambda x: to_class(VsCodeCmakeKits, x), x)
