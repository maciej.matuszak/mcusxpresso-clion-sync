from pathlib import Path
from typing import Union, Dict

from bs4 import BeautifulSoup

from mcusxpresso_clion_sync.CLionCMakeConfig import CLionCMakeConfig

PROJECT_VERSION = '4'


class CLionCMakeSettings:

    def __init__(self, settings_file: Union[str, Path]):
        if isinstance(settings_file, Path):
            self.settings_file = settings_file
        else:
            self.settings_file = Path(settings_file)

        self.configurations: Dict[str, CLionCMakeConfig] = {}

    def reset(self):
        self.configurations.clear()

    def add_cmake_config(self, config_name: str) -> CLionCMakeConfig:
        config = CLionCMakeConfig()
        config.profile_name = config_name
        self.configurations[config_name] = config
        return config

    def update_xml(self) -> BeautifulSoup:
        bs = BeautifulSoup('<?xml version="1.0" encoding="UTF-8"?>', features="xml")
        project = bs.new_tag('project', attrs={'version': PROJECT_VERSION})
        bs.append(project)

        component = bs.new_tag('component', attrs={'name': 'CMakeSharedSettings'})
        project.append(component)

        configurations = bs.new_tag('configurations')
        component.append(configurations)
        for config in self.configurations.values():
            configuration = bs.new_tag('configuration')
            config.update_xml(bs, configuration)
            configurations.append(configuration)
        return bs

    def read(self) -> BeautifulSoup:

        with self.settings_file.open('r') as f:
            data = f.read()
            bs = BeautifulSoup(data, 'xml')
            return bs

    def write(self):
        if not self.settings_file.parent.exists():
            self.settings_file.parent.mkdir(parents=True)
        with self.settings_file.open('wb') as f:
            xml_data = self.update_xml()
            s = xml_data.prettify('UTF-8')
            f.write(s)
