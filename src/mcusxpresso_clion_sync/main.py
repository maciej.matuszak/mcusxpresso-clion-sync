from argparse import ArgumentParser

from mcusxpresso_clion_sync.MCUXpressoSync import MCUXpressoSync


def main():
    parser = ArgumentParser(
        prog='MCUXpressoToClionSync',
        description='This utility sync MCUXpresso VS Code project settings with CLion',
        epilog='Text at the bottom of help')
    parser.add_argument('project_dir')

    args = parser.parse_args()
    syncer = MCUXpressoSync(args.project_dir)
    syncer.run()


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    main()
